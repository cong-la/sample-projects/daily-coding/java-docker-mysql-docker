package com.simsoft.api.sample02.repository;

import com.simsoft.api.sample02.model.SchoolEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface SchoolEventRepository extends JpaRepository<SchoolEvent, Integer> {

    public ArrayList<SchoolEvent> findAll();

    public SchoolEvent findByEventId(int id);

    public SchoolEvent save(SchoolEvent schoolEvent);
}
