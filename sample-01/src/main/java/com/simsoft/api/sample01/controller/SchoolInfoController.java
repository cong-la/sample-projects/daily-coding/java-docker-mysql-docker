package com.simsoft.api.sample01.controller;

import com.simsoft.api.sample01.model.SchoolInfo;
import com.simsoft.api.sample01.repository.SchoolInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(path = "/school")
public class SchoolInfoController {

    @Autowired
    private SchoolInfoRepository schoolInfoRepository;

    @GetMapping()
    public ArrayList<SchoolInfo> findAll() {
        return this.schoolInfoRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public SchoolInfo findById(@PathVariable int id) {
        return this.schoolInfoRepository.findById(id);
    }

    @PostMapping()
    public SchoolInfo save(@RequestBody SchoolInfo schoolInfo) {
        return this.schoolInfoRepository.save(schoolInfo);
    }

}
