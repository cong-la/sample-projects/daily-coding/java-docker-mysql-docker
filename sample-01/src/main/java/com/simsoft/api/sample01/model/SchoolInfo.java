package com.simsoft.api.sample01.model;

import lombok.*;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "school_info")
public class SchoolInfo {

    @Id
    @Column(name = "school_id")
    private int schoolId;

    @Column(name = "name")
    private String name;

}
